<?php

require_once "../priceguide/PlayersInput.php";
require_once "../priceguide/PlayersOutput.php";

require_once "../priceguide/Request.php";
require_once "../priceguide/Results.php";

require_once "../priceguide/PlayerLoader.php";
require_once "../priceguide/ValuesBuilder.php";
require_once "../priceguide/DollarBuilder.php";

$cols = array("mlbamID", "playerName", "defaultPos", "dollarValue", "AB", "HR", "SB", "R", "RBI", "AVG", "IP", "W", "S", "K", "ERA", "WHIP", "MVP", "1stRd", "bestPos");

unlink("stats/1990Results.csv");
$outstream = fopen("stats/1990Results.csv", 'a');

fwrite($outstream, "year,");
foreach($cols as $col)
{
	fwrite($outstream, $col . ",");
}
fwrite($outstream, "\n");
	
	
for ($year = 1990; $year < 1991; $year++)
{
	$request = buildRequest($year);
	$results = processRequest($request, false);

	// Determine awards
	$foundC = false;
	$found1B = false;
	$found2B = false;
	$found3B = false;
	$foundSS = false;
	$foundOF = false;
	foreach ($results->hitters as &$player)
	{
		$player["MVP"] = "";
		if ($player["dollarValue"] >= 30)
		{
			$player["1stRd"] = 1;
		}
		else
		{
			$player["1stRd"] = "";
		}
		
		if ($player["defaultPos"] == "C" && !$foundC)
		{
			$foundC = true;
			$player["bestPos"] = 1;
		}
		elseif ($player["defaultPos"] == "1B" && !$found1B)
		{
			$found1B = true;
			$player["bestPos"] = 1;
		}
		elseif ($player["defaultPos"] == "2B" && !$found2B)
		{
			$found2B = true;
			$player["bestPos"] = 1;
		}
		elseif ($player["defaultPos"] == "3B" && !$found3B)
		{
			$found3B = true;
			$player["bestPos"] = 1;
		}
		elseif ($player["defaultPos"] == "SS" && !$foundSS)
		{
			$foundSS = true;
			$player["bestPos"] = 1;
		}
		elseif ($player["defaultPos"] == "OF" && !$foundOF)
		{
			$foundOF = true;
			$player["bestPos"] = 1;
		}
		else
		{
			$player["bestPos"] = "";
		}
	}
	$results->hitters[0]["MVP"] = 1;
		
	$foundSP = false;
	$foundRP = false;
	foreach ($results->pitchers as &$player)
	{
		$player["MVP"] = "";
		if ($player["dollarValue"] >= 30)
		{
			$player["1stRd"] = 1;
		}
		else
		{
			$player["1stRd"] = "";
		}
		
		if ($player["defaultPos"] == "SP" && !$foundSP)
		{
			$foundSP = true;
			$player["bestPos"] = 1;
		}
		elseif ($player["defaultPos"] == "RP" && !$foundRP)
		{
			$foundRP = true;
			$player["bestPos"] = 1;
		}
		else
		{
			$player["bestPos"] = "";
		}
	}
	$results->pitchers[0]["MVP"] = 1;
	
	// Output the results
	foreach($results->hitters as $player)
	{ 
		$player["dollarValue"] = round($player["dollarValue"]);
		$player["AVG"] = round($player["AVG"], 3);
		
		fwrite($outstream, $year . ",");
		foreach($cols as $col)
		{
			if (isset($player[$col]))
			{
				fwrite($outstream, $player[$col] . ",");
			}
			else
			{
				fwrite($outstream, ",");
			}
		}
		fwrite($outstream, "\n");
	}
	foreach($results->pitchers as $player)
	{
		$player["dollarValue"] = round($player["dollarValue"]);
		$player["HR"] = "";
		$player["R"] = "";
		$player["ERA"] = round(($player["ERA"]*9), 2);
		$player["WHIP"] = round($player["WHIP"], 2);
		
		
		fwrite($outstream, $year . ",");
		foreach($cols as $col)
		{
			if (isset($player[$col]))
			{
				fwrite($outstream, $player[$col] . ",");
			}
			else
			{
				fwrite($outstream, ",");
			}
		}
		fwrite($outstream, "\n");
	}
}

fclose($outstream);

$totalValue = $results->hittersOutput->totalValue + $results->pitchersOutput->totalValue;
print(number_format(($results->hittersOutput->totalValue / $totalValue) * 100));
print(" ");
print(number_format(($results->pitchersOutput->totalValue / $totalValue) * 100));


function buildHittingCategories()
{
   $categories = array();
   
   $categories[] = "HR";
   $categories[] = "SB";
   $categories[] = "R";
   $categories[] = "RBI";
   $categories[] = "xAVG";

   return $categories;
}

function buildPitchingCategories()
{
   $categories = array();

   $categories[] = "W";
   $categories[] = "S";
   $categories[] = "K";
   $categories[] = "xERA";
   $categories[] = "xWHIP";

   return $categories;
}

function buildRequest($year)
{
   $request = new Request();

   $request->dataset       = $year . "S";
   $request->numberOfTeams = 12;
   $request->moneyPerTeam  = 260;
   $request->league        = "MLB";
   $request->minimumBid    = 1;
   $request->outputAsCSV       = true;
   $request->outputAsSimpleCSV = false;

   $request->useTopPosition = true;
   $request->adjustPlayingTime = false;
   $request->useCustomSplit = false;

   if ($request->useCustomSplit)
   {
      $request->hittersSplit = .7;
      $request->pitchersSplit = .3;
   }

   // These need to be in descending order of value.
   $request->hittersInput->positions["C"]    = 24;
   $request->hittersInput->positions["SS"]   = 12;
   $request->hittersInput->positions["2B"]   = 12;
   $request->hittersInput->positions["3B"]   = 12;
   $request->hittersInput->positions["CF"]   = 0;
   $request->hittersInput->positions["LF"]   = 0;
   $request->hittersInput->positions["RF"]   = 0;
   $request->hittersInput->positions["OF"]   = 60;
   $request->hittersInput->positions["1B"]   = 12;
   $request->hittersInput->positions["MI"]   = 12;
   $request->hittersInput->positions["CI"]   = 12;
   $request->hittersInput->positions["IF"]   = 0;
   $request->hittersInput->positions["Util"] = 12;

   $request->hittersInput->minGames["C"]    = 20;
   $request->hittersInput->minGames["1B"]   = 20;
   $request->hittersInput->minGames["2B"]   = 20;
   $request->hittersInput->minGames["3B"]   = 20;
   $request->hittersInput->minGames["SS"]   = 20;
   $request->hittersInput->minGames["OF"]   = 20;
   $request->hittersInput->minGames["LF"]   = 20;
   $request->hittersInput->minGames["CF"]   = 20;
   $request->hittersInput->minGames["RF"]   = 20;
   $request->hittersInput->minGames["CI"]   = 20;
   $request->hittersInput->minGames["MI"]   = 20;
   $request->hittersInput->minGames["IF"]   = 20;
   $request->hittersInput->minGames["Util"] = 1;

   $request->hittersInput->categories = buildHittingCategories();
   $request->hittersInput->numberOfPlayersDrafted  = array_sum($request->hittersInput->positions);

   $request->pitchersInput->positions["SP"]  = 6 * $request->numberOfTeams;
   $request->pitchersInput->positions["RP"]  = 3 * $request->numberOfTeams;
   $request->pitchersInput->positions["P"]   = 0 * $request->numberOfTeams;

   $request->pitchersInput->minGames["SP"]   = 5;
   $request->pitchersInput->minGames["RP"]   = 5;
   $request->pitchersInput->minGames["P"]    = 1;

   $request->pitchersInput->categories = buildPitchingCategories();
   $request->pitchersInput->numberOfPlayersDrafted = array_sum($request->pitchersInput->positions);

   return $request;
}

function processRequest($request, $isHTMLOutput)
{
	
   $results = new Results();

   // Load the players from file
   $loader = new PlayerLoader();
   $results->hitters = $loader->loadPlayers($request, "Hitters", $isHTMLOutput);
   $results->pitchers = $loader->loadPlayers($request, "Pitchers", $isHTMLOutput);

   // Build the values for hitters
   $builder = new ValuesBuilder();
   $results->hittersOutput = $builder->buildValues($results->hitters, $request->hittersInput);

   // Build the values for pitchers
   $builder = new ValuesBuilder();
   $results->pitchersOutput = $builder->buildValues($results->pitchers, $request->pitchersInput);

   $results->totalIP = 0;
   for ($i = 0; $i < $request->pitchersInput->numberOfPlayersDrafted; $i++)
   {
	  if ($results->pitchers[$i]["IP"] > 0)
	  {
		 $results->totalIP += $results->pitchers[$i]["IP"];
	  }
   }
   if ($request->numberOfTeams > 0)
   {
	  $results->totalIP /= $request->numberOfTeams;
   }

   // Come up with some dollar values
   $builder = new DollarBuilder();
   $builder->buildDollars($request, $results);

   return $results;	

}
