<?php 

set_time_limit(0);

$TR_MATCH = "|<tr class=\"full_table.*?>(.*?)</tr>|";

for ($year = 1980; $year < 2020; $year++)
{
	$ch = curl_init("https://www.baseball-reference.com/leagues/MLB/" . $year . "-standard-batting.shtml");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	$html = curl_exec($ch);
	$players = array();
	$j = 0;
	$previousId = "";

	preg_match_all($TR_MATCH, $html, $playerMatches);
	for ($i = 0; $i < count($playerMatches[1]); $i++)
	{
	   preg_match("|<td class=\"left \" data-append-csv=\"(.*?)\"|", $playerMatches[1][$i], $idMatch);
	   preg_match("|<a href=\"/players/.*?>(.*?)</a>|", $playerMatches[1][$i], $nameMatch);
	   preg_match("|<a href=\"/teams/.*?>(.*?)</a>|", $playerMatches[1][$i], $teamMatch);
	   
	   if ($idMatch[1] != $previousId)
	   {
		   $previousId = $idMatch[1];
		   $PA = findStat($playerMatches[1][$i], "PA");
		   $positionString = findStat($playerMatches[1][$i], "pos_summary");
		   if (($PA > 0) && !isPitcher($positionString))
		   {
			   // not really the mlbamID right now (it's bbref's id), but we'll change them all later
			   $players[$j]["mlbamID"] = $idMatch[1];
			   $players[$j]["playerName"] = str_replace("&nbsp;", " ", $nameMatch[1]);
			   $players[$j]["team"] = $teamMatch[1];
			   $players[$j]["league"] = findStat($playerMatches[1][$i], "lg_ID");
			   $players[$j]["G"] = findStat($playerMatches[1][$i], "G");
			   $players[$j]["AB"] = findStat($playerMatches[1][$i], "AB");
			   $players[$j]["R"] = findStat($playerMatches[1][$i], "R");
			   $players[$j]["H"] = findStat($playerMatches[1][$i], "H");
			   $players[$j]["2B"] = findStat($playerMatches[1][$i], "2B");
			   $players[$j]["3B"] = findStat($playerMatches[1][$i], "3B");
			   $players[$j]["HR"] = findStat($playerMatches[1][$i], "HR");
			   $players[$j]["RBI"] = findStat($playerMatches[1][$i], "RBI");
			   $players[$j]["BB"] = findStat($playerMatches[1][$i], "BB");
			   $players[$j]["SO"] = findStat($playerMatches[1][$i], "SO");
			   $players[$j]["SB"] = findStat($playerMatches[1][$i], "SB");
			   $players[$j]["CS"] = findStat($playerMatches[1][$i], "CS");
			   $players[$j]["HBP"] = findStat($playerMatches[1][$i], "HBP");
			   $players[$j]["SH"] = findStat($playerMatches[1][$i], "SH");
			   $players[$j]["SF"] = findStat($playerMatches[1][$i], "SF");
			   $players[$j]["defaultPos"] = parseHitterPositions($positionString);
			   $j++;
		   }
	   }

	}

	convertPlayerIDs($players);
	outputPlayers($players, "stats/" . $year . "Batting.csv");

	$ch = curl_init("https://www.baseball-reference.com/leagues/MLB/" . $year . "-standard-pitching.shtml");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	$html = curl_exec($ch);
	$players = array();
	$j = 0;
	$previousId = "";

	preg_match_all($TR_MATCH, $html, $playerMatches);
	for ($i = 0; $i < count($playerMatches[1]); $i++)
	{
	   preg_match("|<td class=\"left \" data-append-csv=\"(.*?)\"|", $playerMatches[1][$i], $idMatch);
	   preg_match("|<a href=\"/players/.*?>(.*?)</a>|", $playerMatches[1][$i], $nameMatch);
	   preg_match("|<a href=\"/teams/.*?>(.*?)</a>|", $playerMatches[1][$i], $teamMatch);
	   
	   if ($idMatch[1] != $previousId)
	   {
		   $previousId = $idMatch[1];
		   // not really the mlbamID right now (it's bbref's id), but we'll change them all later
		   $players[$j]["mlbamID"] = $idMatch[1];
		   $players[$j]["playerName"] = str_replace("&nbsp;", " ", $nameMatch[1]);
		   $players[$j]["team"] = $teamMatch[1];
		   $players[$j]["league"] = findStat($playerMatches[1][$i], "lg_ID");
		   $players[$j]["G"] = findStat($playerMatches[1][$i], "G");
		   $players[$j]["GS"] = findStat($playerMatches[1][$i], "GS");
		   $players[$j]["W"] = findStat($playerMatches[1][$i], "W");
		   $players[$j]["L"] = findStat($playerMatches[1][$i], "L");
		   $players[$j]["SV"] = findStat($playerMatches[1][$i], "SV");
		   $players[$j]["CG"] = findStat($playerMatches[1][$i], "CG");
		   $players[$j]["SHO"] = findStat($playerMatches[1][$i], "SHO");
		   $players[$j]["H"] = findStat($playerMatches[1][$i], "H");
		   $players[$j]["R"] = findStat($playerMatches[1][$i], "R");
		   $players[$j]["ER"] = findStat($playerMatches[1][$i], "ER");
		   $players[$j]["HR"] = findStat($playerMatches[1][$i], "HR");
		   $players[$j]["BB"] = findStat($playerMatches[1][$i], "BB");
		   $players[$j]["K"] = findStat($playerMatches[1][$i], "SO");
		   $players[$j]["IP"] = findStat($playerMatches[1][$i], "IP");
		   if ($players[$j]["GS"] >= 5)
		   {
				$players[$j]["defaultPos"] = "SP";
		   }
		   else
		   {
			    $players[$j]["defaultPos"] = "RP";
		   }
		   $j++;
	   }

	}
	convertPlayerIDs($players);
	outputPlayers($players, "stats/" . $year . "Pitching.csv");
}

function convertPlayerIDs(&$players)
{
	$ids = array();
	if (($handle = fopen("stats/people2019.csv", "r")) !== FALSE)
	{
		while (($data = fgetcsv($handle)) !== FALSE)
		{
			$ids[$data[2]] = $data[0];
		}
	}
	
	foreach ($players as &$player)
	{
		$player["mlbamID"] = $ids[$player["mlbamID"]];
	}
}

function findStat($html, $statName)
{
	preg_match("|<td.*?data-stat=\"" . $statName . "\" >(.*?)</td>|", $html, $statMatch);
	
	if ($statName = "IP")
	{
	   $ip = $statMatch[1];
       $ip = str_replace(".1", ".3", $ip);
       $ip = str_replace(".2", ".7", $ip);
	   return $ip;
	}
	if ($statName = "lg_ID")
	{
		return $statMatch[1];
	}
	elseif ($statName = "pos_summary")
	{
		return $statMatch[1];
	}
	else
	{
		return intval($statMatch[1]);
	}
}

function outputPlayers($players, $fileName)
{
   
   $outstream = fopen($fileName, 'w');
   
   if (!$players[0]["defaultPos"])
   {
      $players[0]["defaultPos"] = "";
   }

   foreach ($players[0] as $col => $value)
   {
      fwrite($outstream, $col . ",");
   }
   

   fwrite($outstream, "\n");
   
   foreach ($players as $player)
   {
      foreach ($player as $col)
      {
         fwrite($outstream, $col . ",");
      }
      fwrite($outstream, "\n");
   }
   
   fclose($outstream);
}

function isPitcher($positionString)
{
	if (substr($positionString, 0, 1) === "1")
	{
		return true;
	}
	elseif ($positionString == "/1")
	{
		return true;
	}
	else
	{
		return false;
	}
}

function parseHitterPositions($positionString)
{
	// Throw out positions listed after "/" -- which indicates less than 10 games
	if (strpos($positionString, "/") !== false)
	{
		$positionString = substr($positionString, 0, strpos($positionString, "/"));
	}
	
	if (strpos($positionString, "2") !== false)
	{
		return "C";
	}
	elseif (strpos($positionString, "6") !== false)
	{
		return "SS";
	}
	elseif (strpos($positionString, "4") !== false)
	{
		return "2B";
	}
	elseif (strpos($positionString, "5") !== false)
	{
		return "3B";
	}
	elseif (strpos($positionString, "7") !== false)
	{
		return "OF";
	}
	elseif (strpos($positionString, "8") !== false)
	{
		return "OF";
	}
	elseif (strpos($positionString, "9") !== false)
	{
		return "OF";
	}
	elseif (strpos($positionString, "3") !== false)
	{
		return "1B";
	}
	else
	{
		return "";
	}

}

?>