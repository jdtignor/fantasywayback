<?php

$hitterOutput = fopen('stats/AllTimeBatting.csv', 'w');
$pitcherOutput = fopen('stats/AllTimePitching.csv', 'w');

for ($year = 1980; $year < 2020; $year++)
{
	if (($handle = fopen("stats/" . $year . "Batting.csv", "r")) !== FALSE)
	{
		while ($data = fgetcsv($handle))
		{
			// Ignore the header rows except for the first year
			if ($data[0] != "mlbamID")
			{
				// We'll append the year onto the player ID, since we're including multiple seasons from each player
				$data[0] .= "-" . $year;

				fputcsv($hitterOutput, $data);
			}
			elseif ($year == 1980)
			{
				fputcsv($hitterOutput, $data);
			}

		}
	}
	
	
	if (($handle = fopen("stats/" . $year . "Pitching.csv", "r")) !== FALSE)
	{
		while ($data = fgetcsv($handle))
		{
			// Ignore the header rows except for the first year
			if ($data[0] != "mlbamID")
			{
				// We'll append the year onto the player ID, since we're including multiple seasons from each player
				$data[0] .= "-" . $year;

				fputcsv($pitcherOutput, $data);
			}
			elseif ($year == 1980)
			{
				fputcsv($pitcherOutput, $data);
			}
		}
	}
}

fclose($hitterOutput);
fclose($pitcherOutput);

?>