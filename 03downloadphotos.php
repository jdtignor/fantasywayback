<?php 

set_time_limit(0);


// Load all the playerIDs into one big array
if (($handle = fopen("stats/FantasyHOF.csv", "r")) !== FALSE)
{
    while (($data = fgetcsv($handle)) !== FALSE)
	{
		$playerIds[] = $data[1];
	}
	fclose($handle);
}


// Eliminate duplicate ids
$playerIds = array_merge(array_flip(array_flip($playerIds)));


// Get a picture for each playerID
foreach ($playerIds as $playerId)
{
	$filename = "img/" . $playerId . ".jpg";
	
	if (!file_exists($filename))
	{
		$ch = curl_init("https://securea.mlb.com/mlb/images/players/head_shot/" . $playerId . ".jpg");

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		$img = curl_exec($ch);
		curl_close($ch);
		
		$imgFile = fopen($filename, 'x');
		fwrite($imgFile, $img);
		fclose($imgFile);
	}
	
}

?>