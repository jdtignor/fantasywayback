<?php 

// Load each season
for ($year = 1980; $year < 2020; $year++)
{
	$season = array();
	if (($handle = fopen("stats/FantasyHOF.csv", "r")) !== FALSE)
	{
		if (!feof($handle))
		{
			$columns = fgetcsv($handle);
		}

		$i = 0;
		while (!feof($handle))
		{
			$player = fgetcsv($handle);
			if ($player > "" && $player[0] == $year)
			{
				for ($j = 0; $j < count($player); $j++)
				{
					$season[$i][$columns[$j]] = $player[$j];
				}
				$i++;
			}
		}
		fclose($handle);
	}
	
	
	// Split pitchers and hitters
	$hitters = array();
	$pitchers = array();
	foreach ($season as $player)
	{
		if ($player["dollarValue"] >= 1)
		{
			if ( ($player["defaultPos"] != "SP") && ($player["defaultPos"] != "RP") )
			{
				$hitters[] = $player;
			}
			else
			{
				$pitchers[] = $player;
			}
		}
	}


ob_start();

?>
<!DOCTYPE html>
<html>
<head>

<title><?php print($year); ?> Fantasy Baseball Stats</title>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bulma.min.css">
<link rel="stylesheet" href="../css/style.css">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@800&display=swap" rel="stylesheet">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-QS02VEN6GY"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-QS02VEN6GY');
</script>

</head>
<body>

<nav class="navbar is-dark">
	<div class="navbar-brand"><a class="navbar-item logo" href="../index.html">Fantasy Wayback</a>
		<div class="navbar-item">
<div class="field">
	<div class="control">
		<input class="input" id="search" type="text" placeholder="Search..." autocomplete="off" oninput="playerSearch()">
	</div>
	<div class="box" id="results"></div>
</div>
		</div>
	</div>
</nav>

  <section class="section">
    <div class="container">
        <div class="box">
    
    <h1 class="title"><?php print($year); ?> Fantasy Baseball Stats</h1>

      
<!-- PITCHER/HITTER TABS -->
<div class="tabs is-boxed">
<ul>
  <li id="tabBatting" class="is-active"><a href="">Batting</a></li>
  <li id="tabPitching"><a href="">Pitching</a></li>
</ul>
</div>

<div class="table-container" id="divBatting">
<table class="table is-hoverable js-sort-table">
	<tr>
		<th class="js-sort-none"> </th>
		<th>Player </th>
		<th class="js-sort-number">$ </th>
		<th>Pos </th>
		<th class="js-sort-number">AB </th>
		<th class="js-sort-number">HR </th>
		<th class="js-sort-number">SB </th>
		<th class="js-sort-number">R </th>
		<th class="js-sort-number">RBI </th>
		<th class="js-sort-number">AVG </th>
	</tr>

<?php

foreach ($hitters as $player)
{

?>
	<tr>
		<td>
<?php
if ($player["MVP"])
{
	print "<img src=\"../img/mvp.svg\" width=\"16\" height=\"16\" title=\"Fantasy MVP\" />";
}
else
{
	if ($player["1stRd"])
	{
		print "<img src=\"../img/1stRd.svg\" width=\"16\" height=\"16\" title=\"Top Tier Hitter ($30+)\" />";
	}
	if ($player["bestPos"])
	{
		print "<img src=\"../img/bestPos.svg\" width=\"16\" height=\"16\" title=\"Best at Position\" />";
	}
}
?>
		</td>
		<td><a href="<?php print("../" . getPlayerFileName($player["playerName"], $player["mlbamID"])); ?>"><?php print $player["playerName"]; ?></a></td>
		<td><?php print "$" . $player["dollarValue"]; ?></td>
		<td><?php print $player["defaultPos"]; ?></td>
		<td><?php print $player["AB"]; ?></td>
		<td><?php print $player["HR"]; ?></td>
		<td><?php print $player["SB"]; ?></td>
		<td><?php print $player["R"]; ?></td>
		<td><?php print $player["RBI"]; ?></td>
		<td><?php print number_format($player["AVG"], 3); ?></td>
	</tr>

<?php

}

?>

</table>
</div>

<div class="table-container" id="divPitching">
<table class="table is-hoverable js-sort-table">
	<tr>
		<th class="js-sort-none"> </th>
		<th>Player </th>
		<th class="js-sort-number">$ </th>
		<th>Pos </th>
		<th class="js-sort-number">IP </th>
		<th class="js-sort-number">W </th>
		<th class="js-sort-number">S </th>
		<th class="js-sort-number">K </th>
		<th class="js-sort-number">ERA </th>
		<th class="js-sort-number">WHIP </th>
	</tr>

<?php

foreach ($pitchers as $player)
{

?>
	<tr>
		<td>
<?php
if ($player["MVP"])
{
	print "<img src=\"../img/mvp.svg\" width=\"16\" height=\"16\" title=\"Fantasy Cy Young\" />";
}
else
{
	if ($player["1stRd"])
	{
		print "<img src=\"../img/1stRd.svg\" width=\"16\" height=\"16\" title=\"Top Tier Pitcher ($30+)\" />";
	}
	if ($player["bestPos"] && $player["defaultPos"] == "RP")
	{
		print "<img src=\"../img/bestPos.svg\" width=\"16\" height=\"16\" title=\"Best at Position\" />";
	}
}
?>
		</td>
		<td><a href="<?php print("../" . getPlayerFileName($player["playerName"], $player["mlbamID"])); ?>"><?php print $player["playerName"]; ?></a></td>
		<td><?php print "$" . $player["dollarValue"]; ?></td>
		<td><?php print $player["defaultPos"]; ?></td>
		<td><?php print $player["IP"]; ?></td>
		<td><?php print $player["W"]; ?></td>
		<td><?php print $player["S"]; ?></td>
		<td><?php print $player["K"]; ?></td>
		<td><?php print number_format($player["ERA"], 2); ?></td>
		<td><?php print number_format($player["WHIP"], 2); ?></td>
	</tr>

<?php

}

?>

</table>
</div>

            </div>
		</div>
	</section>

	<footer class="footer has-background-dark">
		<p class="has-text-centered has-text-light">
			Dollar values and site design by <a href="https://twitter.com/MaysCopeland">Mays Copeland</a>.
		</p>
	</footer>

    <script src="../js/players.js"></script>
    <script src="../js/seasons.js"></script>
    <script src="../js/sort-table.min.js"></script>
</body>
</html>

<?php		

	file_put_contents("public/seasons/" . $year . ".html", ob_get_contents());

	ob_end_clean();

}

function sortByDollarValue($first, $second)
{

   if ($first["dollarValue"] == $second["dollarValue"])
   {
      return 0;
   }
   elseif ($first["dollarValue"] < $second["dollarValue"])
   {
      return 1;
   }
   else
   {
      return -1;
   }
}

function getPlayerFileName($playerName, $mlbamID)
{
	$filename = $playerName;
	
	// Replace periods with "-"
	$filename = str_replace(".", "-", $filename);
	
	// Replace apostrophes with "-"
	$filename = str_replace("'", "-", $filename);
	
	// Replace spaces with "-"
	$filename = str_replace(" ", "-", $filename);
	
	// Append player ID
	$filename = "players/" . $filename . "-" . $mlbamID . ".html";
	
	// Replace duplicate "--"
	$filename = str_replace("--", "-", $filename);
	
	return $filename;
}

?>