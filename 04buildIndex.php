<?php 

// Load all players into $playerSeasons array
if (($handle = fopen("stats/FantasyHOF.csv", "r")) !== FALSE)
{
    if (!feof($handle))
	{
		$columns = fgetcsv($handle);
	}

	$i = 0;
	while (!feof($handle))
	{
		$player = fgetcsv($handle);
		if ($player > "")
		{
			for ($j = 0; $j < count($player); $j++)
			{
				$playerSeasons[$i][$columns[$j]] = $player[$j];
			}
			$i++;
		}
	}
	fclose($handle);
}

// Calculate career leaders
$playerCareers = array();
foreach ($playerSeasons as $playerSeason)
{
	$foundPlayer = false;
	foreach($playerCareers as &$playerCareer)
	{
		// If we already have a season for this player, add the current dollar value to his total
		if ($playerSeason["mlbamID"] == $playerCareer["mlbamID"])
		{
			$foundPlayer = true;
			$playerCareer["dollarValue"] += $playerSeason["dollarValue"];
			break;
		}
		
	}
	
	// If this is the first season for a player, create a new entry on the career array
	if (!$foundPlayer)
	{
		$playerCareer = array();
		$playerCareer["mlbamID"] = $playerSeason["mlbamID"];
		$playerCareer["playerName"] = $playerSeason["playerName"];
		$playerCareer["dollarValue"] = intval($playerSeason["dollarValue"]);
		$playerCareer["defaultPos"] = $playerSeason["defaultPos"];
		$playerCareers[] = $playerCareer;	
	}

}
// Sort by greatest dollar value
usort($playerCareers, "sortByDollarValue");

// Grab the top 20 hitters
$foundCount = 0;
for ($i = 0; $foundCount < 20; $i++)
{
	if ( ($playerCareers[$i]["defaultPos"] != "SP") && ($playerCareers[$i]["defaultPos"] != "RP") )
	{
		$hitterCareers[] = $playerCareers[$i];
		$foundCount++;
	}
}


// Grab the top 20 pitchers
$foundCount = 0;
for ($i = 0; $foundCount < 20; $i++)
{
	if ( ($playerCareers[$i]["defaultPos"] == "SP") || ($playerCareers[$i]["defaultPos"] == "RP") )
	{
		$pitcherCareers[] = $playerCareers[$i];
		$foundCount++;
	}
}
	

//--------------------------------
// Calculate single season leaders
//--------------------------------
usort($playerSeasons, "sortByDollarValue");

// Grab the top 20 hitters
$foundCount = 0;
for ($i = 0; $foundCount < 20; $i++)
{
	if ( ($playerSeasons[$i]["defaultPos"] != "SP") && ($playerSeasons[$i]["defaultPos"] != "RP") )
	{
		$hitterSeasons[] = $playerSeasons[$i];
		$foundCount++;
	}
}


// Grab the top 20 pitchers
$foundCount = 0;
for ($i = 0; $foundCount < 20; $i++)
{
	if ( ($playerSeasons[$i]["defaultPos"] == "SP") || ($playerSeasons[$i]["defaultPos"] == "RP") )
	{
		$pitcherSeasons[] = $playerSeasons[$i];
		$foundCount++;
	}
}
	
ob_start();

?>
<!DOCTYPE html>
<html>
<head>

<title>Historical Fantasy Baseball Stats</title>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bulma.min.css">
<link rel="stylesheet" href="css/style.css">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@800&display=swap" rel="stylesheet">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<script src="js/players.js"></script>
<script src="js/Chart.bundle.min.js"></script>
<script src="js/drawChart.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-QS02VEN6GY"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-QS02VEN6GY');
</script>

</head>
<body>
<body onload="loadChart()">

<nav class="navbar is-dark">
	<div class="navbar-brand"><a class="navbar-item logo" href="index.html">Fantasy Wayback</a>
		<div class="navbar-item">
<div class="field">
	<div class="control">
		<input class="input" id="search" type="text" placeholder="Search..." autocomplete="off" oninput="playerSearch()">
	</div>
	<div class="box" id="results"></div>
</div>
		</div>
	</div>
</nav>

<section class="hero is-white is-bold">
  <div class="hero-body">
    <div class="container">
		<div class="columns" style="flex-direction:row-reverse">
			<div class="column">
				<h1 class="title is-1">
					Stats and charts for every player in the fantasy era (1980 - today).
				</h1>
			</div>
			<div class="column">
					<canvas id="myChart" width="400" height="200" style="border:1px solid rgba(0,0,0,0.8);background-color:#fff;"></canvas>
					<a id="frontLink" href="players/Mike-Trout-545361.html">
						<div class="columns is-gapless is-mobile" style="align-items: stretch;">
							<div class="column is-narrow heroPlayerImage">
								<img class="image thumbNail" id="frontImage" src="#" onerror="hideImage()" onload="showImage()" />
							</div>
							<div class="column heroPlayerName">
								<span class="title is-5 has-text-light"><span class="title has-text-light" id="frontPlayer"></span> (more...)</span>
							</div>
						</div>
					</a>
			</div>
    </div>
  </div>
</section>

  <section class="section">
    <div class="container">

<div class="columns is-multiline">
	<div class="column">
		<div class="box">
<p class="title">Best Single Season</p>
<p class="subtitle">(Hitters)</p>

<div class="table-container">
<table class="table is-hoverable">
	<tr>
		<th>Year</th>
		<th>Player</th>
		<th>$</th>
		<th>HR</th>
		<th>SB</th>
		<th>R</th>
		<th>RBI</th>
		<th>AVG</th>
	</tr>

<?php

foreach ($hitterSeasons as $season)
{

?>
	<tr>
		<td><a href="<?php print "seasons/" . $season["year"]; ?>.html"><?php print $season["year"]; ?></a></td>
		<td><a href="<?php print(getPlayerFileName($season["playerName"], $season["mlbamID"])); ?>"><?php print $season["playerName"]; ?></a></td>
		<td><?php print "$" . $season["dollarValue"]; ?></td>
		<td><?php print $season["HR"]; ?></td>
		<td><?php print $season["SB"]; ?></td>
		<td><?php print $season["R"]; ?></td>
		<td><?php print $season["RBI"]; ?></td>
		<td><?php print number_format($season["AVG"], 3); ?></td>
	</tr>

<?php

}

?>

</table>
</div>
	
		</div>
	</div>
	<div class="column is-multiline">
		<div class="box">
<p class="title">Best Single Season</p>
<p class="subtitle">(Pitchers)</p>

<div class="table-container">
<table class="table is-hoverable">
	<tr>
		<th>Year</th>
		<th>Player</th>
		<th>$</th>
		<th>W</th>
		<th>S</th>
		<th>K</th>
		<th>ERA</th>
		<th>WHIP</th>
	</tr>

<?php

foreach ($pitcherSeasons as $season)
{

?>
	<tr>
		<td><a href="<?php print "seasons/" . $season["year"]; ?>.html"><?php print $season["year"]; ?></a></td>
		<td><a href="<?php print(getPlayerFileName($season["playerName"], $season["mlbamID"])); ?>"><?php print $season["playerName"]; ?></a></td>
		<td><?php print "$" . $season["dollarValue"]; ?></td>
		<td><?php print $season["W"]; ?></td>
		<td><?php print $season["S"]; ?></td>
		<td><?php print $season["K"]; ?></td>
		<td><?php print number_format($season["ERA"], 2); ?></td>
		<td><?php print number_format($season["WHIP"], 2); ?></td>
	</tr>

<?php

}

?>

</table>
</div>

		</div>
	</div>
</div>

<div class="columns is-centered">
	<div class="column">
		<div class="box">
<p class="title">Career Earnings Leaders</p>
<p class="subtitle">(Hitters)</p>

<div class="table-container">
<table class="table is-hoverable">
	<tr>
		<th>Player</th>
		<th>$</th>
	</tr>

<?php

foreach ($hitterCareers as $career)
{

?>
	<tr>
		<td><a href="<?php print(getPlayerFileName($career["playerName"], $career["mlbamID"])); ?>"><?php print $career["playerName"]; ?></a></td>
		<td><?php print "$" . $career["dollarValue"]; ?></td>
	</tr>

<?php

}

?>
</table>
</div>
	
		</div>
	</div>
	<div class="column">
		<div class="box">
<p class="title">Career Earnings Leaders</p>
<p class="subtitle">(Pitchers)</p>

<div class="table-container">
<table class="table is-hoverable">
	<tr>
		<th>Player</th>
		<th>$</th>
	</tr>

<?php

foreach ($pitcherCareers as $career)
{

?>
	<tr>
		<td><a href="<?php print(getPlayerFileName($career["playerName"], $career["mlbamID"])); ?>"><?php print $career["playerName"]; ?></a></td>
		<td><?php print "$" . $career["dollarValue"]; ?></td>
	</tr>

<?php

}

?>

</table>
</div>

		</div>
	</div>
</div>

		</div>
	</section>
	
	<footer class="footer has-background-dark">
		<p class="has-text-centered has-text-light">
			Dollar values and site design by <a href="https://twitter.com/MaysCopeland">Mays Copeland</a>.
		</p>
	</footer>

</body>
</html>

<?php		

file_put_contents("public/index.html", ob_get_contents());

ob_end_clean();

function sortByDollarValue($first, $second)
{

   if ($first["dollarValue"] == $second["dollarValue"])
   {
      return 0;
   }
   elseif ($first["dollarValue"] < $second["dollarValue"])
   {
      return 1;
   }
   else
   {
      return -1;
   }
}

function getPlayerFileName($playerName, $mlbamID)
{
	$filename = $playerName;
	
	// Replace periods with "-"
	$filename = str_replace(".", "-", $filename);
	
	// Replace apostrophes with "-"
	$filename = str_replace("'", "-", $filename);
	
	// Replace spaces with "-"
	$filename = str_replace(" ", "-", $filename);
	
	// Append player ID
	$filename = "players/" . $filename . "-" . $mlbamID . ".html";
	
	// Replace duplicate "--"
	$filename = str_replace("--", "-", $filename);
	
	return $filename;
}

?>