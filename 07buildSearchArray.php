<?php 

// Load all players into $playerSeasons array
if (($handle = fopen("stats/FantasyHOF.csv", "r")) !== FALSE)
{
    if (!feof($handle))
	{
		$columns = fgetcsv($handle);
	}

	$i = 0;
	while (!feof($handle))
	{
		$player = fgetcsv($handle);
		// Only keep seasons at or above $1
		if (($player > "") && ($player[4] >= 1))
		{
			$playerSeasons[$i]["mlbamID"] = $player[1];
			$playerSeasons[$i]["playerName"] = $player[2];
			$playerSeasons[$i]["defaultPos"] = $player[3];
			$playerSeasons[$i]["dollarValue"] = $player[4];
			$i++;
		}
	}
	fclose($handle);
}

// Calculate career leaders
$playerCareers = array();
foreach ($playerSeasons as $playerSeason)
{
	$foundPlayer = false;
	foreach($playerCareers as &$playerCareer)
	{
		// If we already have a season for this player, add the current dollar value to his total
		if ($playerSeason["mlbamID"] == $playerCareer["mlbamID"])
		{
			$foundPlayer = true;
			$playerCareer["dollarValue"] += $playerSeason["dollarValue"];
			break;
		}
		
	}
	
	// If this is the first season for a player, create a new entry on the career array
	if (!$foundPlayer)
	{
		$playerCareer = array();
		$playerCareer["mlbamID"] = $playerSeason["mlbamID"];
		$playerCareer["playerName"] = $playerSeason["playerName"];
		$playerCareer["dollarValue"] = intval($playerSeason["dollarValue"]);
		$playerCareer["defaultPos"] = $playerSeason["defaultPos"];
		$playerCareers[] = $playerCareer;	
	}

}
// Sort by greatest dollar value
usort($playerCareers, "sortByDollarValue");

foreach ($playerCareers as $player)
{
	print("{n: \"" . $player["playerName"] . "\", ");
	print("i: \"" . $player["mlbamID"] . "\", ");
	print("p: \"" . $player["defaultPos"] . "\"},\r\n");
}

function sortByDollarValue($first, $second)
{

   if ($first["dollarValue"] == $second["dollarValue"])
   {
      return 0;
   }
   elseif ($first["dollarValue"] < $second["dollarValue"])
   {
      return 1;
   }
   else
   {
      return -1;
   }
}

?>