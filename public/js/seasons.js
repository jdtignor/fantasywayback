function showBatting() {  
  document.getElementById('divBatting').style.display = 'block';
  document.getElementById('divPitching').style.display = 'none';

  document.getElementById('tabBatting').classList.add("is-active")
  document.getElementById('tabPitching').classList.remove("is-active")
}

function showPitching() {  
  document.getElementById('divBatting').style.display = 'none';
  document.getElementById('divPitching').style.display = 'block';
  
  document.getElementById('tabBatting').classList.remove("is-active")
  document.getElementById('tabPitching').classList.add("is-active") 
}

window.onload = function() {
  document.getElementById("tabBatting").onclick = function() {
    showBatting();
    return false;
  }
    
  document.getElementById("tabPitching").onclick = function() {
    showPitching();
    return false;
  }
}

showBatting();