function drawChart(years, values){
	
	var ctx = document.getElementById('myChart');
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: years,
			datasets: [{
				data: values,
				fill: true,
				spanGaps: true,
				borderColor: '#000',
				backgroundColor: '#2ecc71',
				borderWidth: 2
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						min: 0,
						suggestedMax: 50,
						callback: function(value, index, values) {
							return '$' + value.toFixed();
						}
					}
				}]
			},
			legend: {
				display: false
			}
		}
	});
}