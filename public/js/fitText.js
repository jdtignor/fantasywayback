function fitText(outputSelector){
    // max font size in pixels
    const maxFontSize = 50;
    // get the DOM output element by its selector
    let outputDiv = document.getElementById(outputSelector);
	
	
    // get element's width
    let width = outputDiv.clientWidth;
    // get content's width
    let contentWidth = outputDiv.scrollWidth;
    // get widthFontSize
    let widthFontSize = parseInt(window.getComputedStyle(outputDiv, null).getPropertyValue('font-size'),10);
    // if content's width is bigger then elements width - overflow
    if (contentWidth > width){
        widthFontSize = Math.ceil(widthFontSize * width/contentWidth,10);
        widthFontSize =  widthFontSize > maxFontSize  ? widthFontSize = maxFontSize  : widthFontSize - 1;
        outputDiv.style.fontSize = widthFontSize+'px';   
    }else{
        // content is smaller then width... let's resize in 1 px until it fits 
        while (contentWidth === width && widthFontSize < maxFontSize){
            widthFontSize = Math.ceil(widthFontSize) + 1;
            widthFontSize = widthFontSize > maxFontSize  ? widthFontSize = maxFontSize  : widthFontSize;
            outputDiv.style.fontSize = widthFontSize+'px';   
            // update widths
            width = outputDiv.clientWidth;
            contentWidth = outputDiv.scrollWidth;
            outputDiv.style.fontSize = widthFontSize-1+'px'
        }
    }
	
    // get element's height
    let height = outputDiv.clientHeight;
    // get content's height
    let contentHeight = outputDiv.scrollHeight;
    // get heightFontSize
    let heightFontSize = parseInt(window.getComputedStyle(outputDiv, null).getPropertyValue('font-size'),10);
    // if content's height is bigger then elements width - height
    if (contentHeight > height){
        heightFontSize = Math.ceil(heightFontSize * height/contentHeight,10);
        heightFontSize =  heightFontSize > maxFontSize  ? heightFontSize = maxFontSize  : heightFontSize - 1;
        outputDiv.style.fontSize = heightFontSize+'px';   
    }else{
        // content is smaller then height... let's resize in 1 px until it fits 
        while (contentHeight === height && heightFontSize < maxFontSize){
            heightFontSize = Math.ceil(heightFontSize) + 1;
            heightFontSize = heightFontSize > maxFontSize  ? heightFontSize = maxFontSize  : heightFontSize;
            outputDiv.style.fontSize = heightFontSize+'px';   
            // update height
            width = outputDiv.clientHeight;
            contentWidth = outputDiv.Height;
            outputDiv.style.fontSize = heightFontSize-1+'px'
        }
    }
	if (widthFontSize < heightFontSize) {
		outputDiv.style.fontSize = widthFontSize-1+'px';
	}else{
		outputDiv.style.fontSize = heightFontSize-1+'px';
	}
}