<?php 

$playerIds = array();
$allSeasons = array();

// Load all the data into one big array
if (($handle = fopen("stats/FantasyHOF.csv", "r")) !== FALSE)
{
	$i = 0;
    while (($data = fgetcsv($handle)) !== FALSE)
	{
		$playerIds[] = $data[1];
		$allSeasons[$i]["mlbamID"] = $data[1];
		$allSeasons[$i]["playerName"] = $data[2];
		$allSeasons[$i]["year"] = intval($data[0]);
		$allSeasons[$i]["defaultPos"] = $data[3];
		
		$dollarValue = $data[4];
		if ($dollarValue < 1)
		{
			$allSeasons[$i]["dollarValue"] = 0;
		}
		else
		{
			$allSeasons[$i]["dollarValue"] = $dollarValue;
		}

		if ($data[3] == "SP" || $data[3] == "RP")
		{
			$allSeasons[$i]["isHitter"] = false;
		}
		else
		{
			$allSeasons[$i]["isHitter"] = true;
		}
		
		if ($allSeasons[$i]["isHitter"] )
		{
			$allSeasons[$i]["AB"] = $data[5];
			$allSeasons[$i]["HR"] = $data[6];
			$allSeasons[$i]["SB"] = $data[7];
			$allSeasons[$i]["R"] = $data[8];
			$allSeasons[$i]["RBI"] = $data[9];
			$allSeasons[$i]["AVG"] = $data[10];
		}
		else
		{
			$allSeasons[$i]["IP"] = $data[11];
			$allSeasons[$i]["W"] = $data[12];
			$allSeasons[$i]["S"] = $data[13];
			$allSeasons[$i]["K"] = $data[14];
			$allSeasons[$i]["ERA"] = $data[15];
			$allSeasons[$i]["WHIP"] = $data[16];
		}
		
		$allSeasons[$i]["MVP"] = $data[17];
		$allSeasons[$i]["1stRd"] = $data[18];
		$allSeasons[$i]["bestPos"] = $data[19];
		
		$i++;
    }
    fclose($handle);
}

// Eliminate duplicate ids
$playerIds = array_merge(array_flip(array_flip($playerIds)));

foreach ($playerIds as $playerId)
{
	// Copy all of one player's seasons into a new array
	$playerSeasons = array();
	unset($firstSeason);
	foreach ($allSeasons as $season)
	{
		if ($season["mlbamID"] == $playerId)
		{
			if (!isset($firstSeason))
			{
				$firstSeason = $season;
				$playerSeasons[] = $season;
			}
			else
			{
				if ($firstSeason["isHitter"] == $season["isHitter"])
				{	
					$playerSeasons[] = $season;
				}
			}
		}
	}
	
	// Make sure the player has a positive year
	$playerIsPositive = false;
	foreach($playerSeasons as $playerSeason)
	{
		if ($playerSeason["dollarValue"] >= 1)
		{
			$playerIsPositive = true;
		}
	}
	
	if ($playerIsPositive)
	{
	
		// Add missing years
		$currentYear = $playerSeasons[0]["year"];
		$fullPlayerSeasons = array();
		foreach ($playerSeasons as $playerSeason)
		{
			while ($currentYear < $playerSeason["year"])
			{
				$dummySeason = array();
				$dummySeason["year"] = $currentYear;
				$dummySeason["defaultPos"] = "";
				$dummySeason["dollarValue"] = "";
				$dummySeason["MVP"] = "";
				$dummySeason["1stRd"] = "";
				$dummySeason["bestPos"] = "";
				
				if ($playerSeasons[0]["isHitter"])
				{
					$dummySeason["AB"] = "";
					$dummySeason["HR"] = "";
					$dummySeason["SB"] = "";
					$dummySeason["R"] = "";
					$dummySeason["RBI"] = "";
					$dummySeason["AVG"] = "";
				}
				else
				{
					$dummySeason["IP"] = "";
					$dummySeason["W"] = "";
					$dummySeason["S"] = "";
					$dummySeason["K"] = "";
					$dummySeason["ERA"] = "";
					$dummySeason["WHIP"] = "";			
				}
				
				$fullPlayerSeasons[] = $dummySeason;
				$currentYear++;
			}
			
			$fullPlayerSeasons[] = $playerSeason;
			$currentYear++;
		}

		
		// Make arrays for chart values
		$chartYears = array();
		$chartValues = array();
		foreach ($fullPlayerSeasons as $season)
		{
			
			$chartYears[] = $season["year"];
			if ($season["dollarValue"] == "")
			{
				$chartValues[] = 0;
			}
			else
			{
				$chartValues[] = $season["dollarValue"];
			}
		}
		
		// Count awards
		$numberOfMVPs = 0;
		$numberOf1stRd = 0;
		$numberOfBestPos = 0;
		$yearOfLastMVP = "";
		$yearOfLast1stRd = "";
		$yearOfLastBestPos = "";
		foreach ($fullPlayerSeasons as $season)
		{
			$numberOfMVPs += $season["MVP"];
			$numberOf1stRd += $season["1stRd"];
			$numberOfBestPos += $season["bestPos"];
			
			if ($season["MVP"])
			{
				$yearOfLastMVP = $season["year"];
			}
			else
			{
				if ($season["1stRd"])
				{
					$yearOfLast1stRd = $season["year"];
				}
				if ($season["bestPos"])
				{
					$yearOfLastBestPos = $season["year"];
				}
			}
		}
		$numberOf1stRd -= $numberOfMVPs;
		$numberOfBestPos -= $numberOfMVPs;
		
		$positions = array();
		// Get positions as string
		foreach ($fullPlayerSeasons as $season)
		{
			if (!in_array($season["defaultPos"], $positions))
			{
				$positions[] = $season["defaultPos"];
			}
		}
		$positionsString = implode("-", $positions);
		$positionsString = ltrim($positionsString, "-");
		$positionsString = rtrim($positionsString, "-");
		$positionsString = str_replace("--", "-", $positionsString);
		
		// Now output one page per player
		ob_start();
		$playerName = $fullPlayerSeasons[0]["playerName"];
		$isHitter = $fullPlayerSeasons[0]["isHitter"];

?>
<!DOCTYPE html>
<html>
<head>

<title><?php echo $playerName; ?> - Fantasy Baseball Stats</title>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bulma.min.css">
<link rel="stylesheet" href="../css/style.css">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@800&display=swap" rel="stylesheet">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<script src="../js/Chart.bundle.min.js"></script>
<script src="../js/players.js"></script>
<script src="../js/fitText.js"></script>
<script src="../js/drawChart.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-QS02VEN6GY"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-QS02VEN6GY');
</script>

</head>

<body>

<nav class="navbar is-dark">
	<div class="navbar-brand"><a class="navbar-item logo" href="../index.html">Fantasy Wayback</a>
		<div class="navbar-item">
<div class="field">
	<div class="control">
		<input class="input" id="search" type="text" placeholder="Search..." autocomplete="off" oninput="playerSearch()">
	</div>
	<div class="box" id="results"></div>
</div>
		</div>
	</div>
</nav>

  <section class="section">
    <div class="container">

	<div class="columns">
	
<?php

	if (file_exists("public/img/" . $playerId . ".jpg"))
	{
?>
		<div class="column is-narrow">
			<div class="cardFrame">
				<img class="cardImg" src="../img/<?php print($playerId); ?>.jpg" height="240" width="160" />
				<div class="cardCaption" id="cardCaption"><?php print($playerName); ?></div>
				<script> fitText("cardCaption"); </script>
			</div>
		</div>

<?php 
	}
?>
		<div class="column">
			<div class="box">
				<canvas id="myChart" width="400" height="200"></canvas>
				<script id="draw">drawChart(<?php echo json_encode($chartYears); ?>, <?php echo json_encode($chartValues); ?>);</script>
			</div>
		</div>
	</div>


	<div class="columns">
		<div class="column">
			<div class="box">
<h1 class="title is-1"><?php print($playerName); ?></h1>
<h3 class="title is-3"><?php print($positionsString); ?></h3>
<?php 
		if ($isHitter)
		{
			if ($numberOfMVPs > 1)
			{
				print("<img src=\"../img/mvp.svg\" width=\"16\" height=\"16\" title=\"Fantasy MVP\" /> " . $numberOfMVPs . "x Fantasy MVP<br />");
			}
			elseif ($numberOfMVPs == 1)
			{
				print("<img src=\"../img/mvp.svg\" width=\"16\" height=\"16\" title=\"Fantasy MVP\" /> " . $yearOfLastMVP . " Fantasy MVP<br />");
			}

			if ($numberOf1stRd > 1)
			{
				print("<img src=\"../img/1stRd.svg\" width=\"16\" height=\"16\" title=\"Top Tier Hitter ($30+)\" /> " . $numberOf1stRd . "x Top Tier Hitter ($30+)<br />");
			}
			elseif ($numberOf1stRd == 1)
			{
				print("<img src=\"../img/1stRd.svg\" width=\"16\" height=\"16\" title=\"Top Tier Hitter ($30+)\" /> " . $yearOfLast1stRd . " Top Tier Hitter ($30+)<br />");
			}
		}
		else
		{
			if ($numberOfMVPs > 1)
			{
				print("<img src=\"../img/mvp.svg\" width=\"16\" height=\"16\" title=\"Fantasy Cy Young\" /> " . $numberOfMVPs . "x Fantasy Cy Young<br />");
			}
			elseif ($numberOfMVPs == 1)
			{
				print("<img src=\"../img/mvp.svg\" width=\"16\" height=\"16\" title=\"Fantasy Cy Young\" /> " . $yearOfLastMVP . " Fantasy Cy Young<br />");
			}

			if ($numberOf1stRd > 1)
			{
				print("<img src=\"../img/1stRd.svg\" width=\"16\" height=\"16\" title=\"Top Tier Pitcher ($30+)\" /> " . $numberOf1stRd . "x Top Tier Pitcher ($30+)<br />");
			}
			elseif ($numberOf1stRd == 1)
			{
				print("<img src=\"../img/1stRd.svg\" width=\"16\" height=\"16\" title=\"Top Tier Pitcher ($30+)\" /> " . $yearOfLast1stRd . " Top Tier Pitcher ($30+)<br />");
			}
		}

		if ($numberOfBestPos > 1)
		{
			print("<img src=\"../img/bestPos.svg\" width=\"16\" height=\"16\" title=\"Best at Position\" /> " . $numberOfBestPos . "x Best at Position<br />");
		}
		elseif ($numberOfBestPos == 1)
		{
			print("<img src=\"../img/bestPos.svg\" width=\"16\" height=\"16\" title=\"Best at Position\" /> " . $yearOfLastBestPos . " Best at Position<br />");
		}

?>
			</div>
		</div>
		<div class="column is-three-fifths">
			<div class="box">
				<div class="table-container">

<?php

if ($isHitter)
{

?>

<table class="table is-hoverable">
	<tr>
		<th> </th>
		<th>Year</th>
		<th>$</th>
		<th>Pos</th>
		<th>AB</th>
		<th>HR</th>
		<th>SB</th>
		<th>R</th>
		<th>RBI</th>
		<th>AVG</th>
	</tr>
	
<?php

	foreach ($fullPlayerSeasons as $playerSeason)
	{
?>
	<tr>
		<td>
<?php
if ($playerSeason["MVP"])
{
	echo "<img src=\"../img/mvp.svg\" width=\"16\" height=\"16\" title=\"Fantasy MVP\" />";
}
else
{
	if ($playerSeason["1stRd"])
	{
		echo "<img src=\"../img/1stRd.svg\" width=\"16\" height=\"16\" title=\"Top Tier Hitter ($30+)\" />";
	}
	if ($playerSeason["bestPos"])
	{
		echo "<img src=\"../img/bestPos.svg\" width=\"16\" height=\"16\" title=\"Best at Position\" />";
	}
}
?>
		</td>
		<td><a href="<?php echo "../seasons/" . $playerSeason["year"]; ?>.html"><?php echo $playerSeason["year"]; ?></a></td>
		<td><?php echo formatDollars($playerSeason["dollarValue"]); ?></td>
		<td><?php echo $playerSeason["defaultPos"]; ?></td>
		<td><?php echo $playerSeason["AB"]; ?></td>
		<td><?php echo $playerSeason["HR"]; ?></td>
		<td><?php echo $playerSeason["SB"]; ?></td>
		<td><?php echo $playerSeason["R"]; ?></td>
		<td><?php echo $playerSeason["RBI"]; ?></td>
		<td><?php echo formatNumber($playerSeason["AVG"], 3); ?></td>
	</tr>
<?php
	}
}
else // is a pitcher
{

?>

<table class="table is-hoverable">
	<tr>
		<th> </th>
		<th>Year</th>
		<th>$</th>
		<th>Pos</th>
		<th>IP</th>
		<th>W</th>
		<th>S</th>
		<th>K</th>
		<th>ERA</th>
		<th>WHIP</th>
	</tr>
	
<?php

	foreach ($fullPlayerSeasons as $playerSeason)
	{
?>
	<tr>
		<td>
<?php
if ($playerSeason["MVP"])
{
	echo "<img src=\"../img/mvp.svg\" width=\"16\" height=\"16\" title=\"Fantasy Cy Young\" />";
}
else
{
	if ($playerSeason["1stRd"])
	{
		echo "<img src=\"../img/1stRd.svg\" width=\"16\" height=\"16\" title=\"Top Tier Pitcher ($30+)\" />";
	}
	if ($playerSeason["bestPos"] && $playerSeason["defaultPos"] == "RP")
	{
		echo "<img src=\"../img/bestPos.svg\" width=\"16\" height=\"16\" title=\"Best at Position\" />";
	}
}
?>
		</td>
		<td><a href="<?php echo "../seasons/" . $playerSeason["year"]; ?>.html"><?php echo $playerSeason["year"]; ?></a></td>
		<td><?php echo formatDollars($playerSeason["dollarValue"]); ?></td>
		<td><?php echo $playerSeason["defaultPos"]; ?></td>
		<td><?php echo $playerSeason["IP"]; ?></td>
		<td><?php echo $playerSeason["W"]; ?></td>
		<td><?php echo $playerSeason["S"]; ?></td>
		<td><?php echo $playerSeason["K"]; ?></td>
		<td><?php echo formatNumber($playerSeason["ERA"], 2); ?></td>
		<td><?php echo formatNumber($playerSeason["WHIP"], 2); ?></td>
	</tr>
<?php
	}
}

?>
</table>
				</div>
			</div>
		</div>
	</div>

		</div>
	</section>

	<footer class="footer has-background-dark">
		<p class="has-text-centered has-text-light">
			Dollar values and site design by <a href="https://twitter.com/MaysCopeland">Mays Copeland</a>.
		</p>
	</footer>

</body>
</html>

<?php

		file_put_contents(getPlayerFileName($playerName, $playerId), ob_get_contents());
		ob_end_clean();
	}
}

function formatDollars($dollarValue)
{
	if ($dollarValue < 1)
	{
		return "";	
	}
	else
	{
		return "$" . $dollarValue;
	}
}

function formatNumber($number, $decimals)
{
	if (!is_numeric($number))
	{
		return "";
	}
	else
	{
		return number_format($number, $decimals);
	}
}

function getPlayerFileName($playerName, $mlbamID)
{
	$filename = $playerName;
	
	// Replace periods with "-"
	$filename = str_replace(".", "-", $filename);
	
	// Replace apostrophes with "-"
	$filename = str_replace("'", "-", $filename);
	
	// Replace spaces with "-"
	$filename = str_replace(" ", "-", $filename);
	
	// Append player ID
	$filename = "public/players/" . $filename . "-" . $mlbamID . ".html";
	
	// Replace duplicate "--"
	$filename = str_replace("--", "-", $filename);
	
	return $filename;
}

?>